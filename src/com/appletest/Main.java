package com.appletest;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    /**
     * com.appletest.Main method ot bootstrap the logic, read the file line by line and create album and songs
     *
     * @param args
     */
    public static void main(String[] args) {

        FileReader fileReader = null;

        try {

            // If the filename is provided use that else use "com.appletest.Album.txt" as the default filename.
            File file = null;
            if (args.length > 0) {
                file = new File(args[0]);
            } else {
                file = new File("Albums.txt");
            }

            fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            List<Album> albums = new ArrayList<>();
            Boolean createNewAlbum = true;
            Album currAlbum = null;

            // Read the file line by line
            while ((line = bufferedReader.readLine()) != null) {

                // If line is empty, add the current album to the list if albums and move to the next line
                if (line.isEmpty()) {
                    createNewAlbum = true;
                    albums.add(currAlbum);
                    continue;
                }

                // New album was set by the empty line. Now set the flag to false and create a new album. Otherwise, the
                // line is a song. Create a song and add it ti the current album.
                if (createNewAlbum) {
                    createNewAlbum = false;
                    String delimiter = "/";
                    String[] tokens = line.split(delimiter);
                    currAlbum = new Album(tokens[1].trim(), tokens[0].trim(), Integer.parseInt(tokens[2].trim()), new ArrayList<Song>());
                } else {
                    String delimiter = "-";
                    String[] tokens = line.split(delimiter);
                    String[] durTokens = tokens[1].split(":");
                    Song song = new Song(tokens[0].trim(), toSeconds(Integer.parseInt(durTokens[0].trim()), Integer.parseInt(durTokens[1].trim())));
                    currAlbum.addSong(song);
                }
            }

            // Close the file reader
            fileReader.close();

            // Print the songs
            printSongs(albums);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fileReader.close();
            } catch (IOException ex) {
                System.err.println("An IOException was caught!");
                ex.printStackTrace();
            }
        }
    }

    /**
     * Iterates through the list of albums and prints all the songs and other info
     *
     * @param albums
     */
    public static void printSongs(List<Album> albums) {
        long totalDuration = 0;

        for (Album album : albums) {
            for (Song song : album.getSongs()) {
                String songInfo = String.format("%s - %s - %02d - %s - %s", album.getArtist(), album.getName(), album.getYear(), song.getTitle(), toFormattedDurationString(song.getLength()));
                System.out.println(songInfo);
                totalDuration += song.getLength();
            }
        }

        System.out.println("Total Time: " + toFormattedDurationString(totalDuration));
    }

    /**
     * Formats seconds to hh:mm:ss format
     *
     * @param totalSecs
     * @return
     */
    public static String toFormattedDurationString(long totalSecs) {
        long hours = totalSecs / 3600;
        long minutes = (totalSecs % 3600) / 60;
        long seconds = totalSecs % 60;

        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    /**
     * Converts minutes and seconds to total seconds.
     *
     * @param minutes
     * @param seconds
     * @return
     */
    public static long toSeconds(int minutes, int seconds) {
        return minutes * 60 + seconds;
    }
}
