package com.appletest;

import java.util.List;

public class Album {

    private String name;
    private String artist;
    private Integer year;
    private List<Song> songs;

    public List<Song> getSongs() {
        return songs;
    }

    public void setSongs(List<Song> songs) {
        this.songs = songs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Album(String name, String artist, Integer year) {
        this.name = name;
        this.artist = artist;
        this.year = year;
    }

    public Album(String name, String artist, Integer year, List<Song> songs) {
        this.name = name;
        this.artist = artist;
        this.year = year;
        this.songs = songs;
    }

    public Album() {
    }

    public void addSong(Song song){
        this.songs.add(song);
    }
}
