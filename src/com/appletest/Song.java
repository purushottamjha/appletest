package com.appletest;

public class Song {
    private String title;
    private long length;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public Song(String title, long length) {
        this.title = title;
        this.length = length;
    }
}
